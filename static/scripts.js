function addEvent(){
	// build an object of event data to submit

	var event = { 
		name: jQuery("#id_name").val(),
		date: jQuery("#id_date").val(),
		hour: jQuery("#id_for_date_hour :selected").val(),
		minute: jQuery("#id_for_date_minute :selected").val(),
		second: jQuery("#id_for_date_second :selected").val(),
		description: jQuery("#id_description").val(),
		patients: jQuery("#id_patients").val() };
	// make request, process response
	jQuery.post("/event/add/", event,
		function(response){
			//jQuery("#event_errors").empty();
			// evaluate the "success" parameter
			if(response.success == "True"){
				// disable the submit button to prevent duplicates
				jQuery("#submit_event").attr('enabled','enabled');
				jQuery("#no_events").empty();
				jQuery("#events").prepend(response.html).slideDown();			
				jQuery("#event_form").slideToggle();		
                                jQuery(".deletion_form").addClass('hidden');	
				jQuery(".del_event").click(function(){                  
							  pos = this.id;
							  jQuery("#deletion_form"+pos).slideDown();
							  //jQuery("#cancel_deletion"+pos).click(slideToggleDelEventForm(id));
							  jQuery("#cancel_deletion"+pos).click(function(){

							  jQuery("#deletion_form"+pos).slideUp();
							    return false;
							  });
							  jQuery("#submit_deletion"+pos).click(function(){
							  delEvent(pos)
								  
							  });

							  });



			}
			else{
				// add the error text to the event_errors div
				jQuery("#submit_event").attr('enabled','enabled');
				jQuery("#event_errors").append(response.html);
			}
		}, "json");
	
}

function delEvent(pos){
	// build an object of event data to submit

	var event = { 
		idd: jQuery("#id_event"+pos).val()
		};
	// make request, process response
	jQuery.post("/event/delete/", event,
		function(response){
			//jQuery("#event_errors").empty();
			// evaluate the "success" parameter
			if(response.success == "True"){
	
				//jQuery("#deletion_form"+pos).slideToggle();	
                                jQuery("#event"+pos).empty();		
			}
			else{
				// add the error text to the event_errors div
				jQuery("#submit_event").attr('enabled','enabled');
				jQuery("#event_errors").append(response.html);
			}
		}, "json");
	
}




// toggles visibility of "write event" link
// and the event form.
function slideToggleEventForm(){
	jQuery("#event_form").slideToggle();
	//jQuery("#add_event").slideToggle();
}

function statusBox(){
	jQuery('<div id="loading">Loading...</div>')
	.prependTo("#main")
	.ajaxStart(function(){jQuery(this).show();})
	.ajaxStop(function(){jQuery(this).hide();})
}

function prepareDocument(){
	//prepare the search box
	jQuery("form#search").submit(function(){
		text = jQuery("#name").val();
		if (text == "" || text == "Search"){
			alert("ΠΑΡΑΚΑΛΩ ΣΥΜΠΛΗΡΩΣΤΕ ΤΟ ΠΕΔΙΟ");
			return false;
		}
	});

	//prepare delete form



	//prepare product event form
	jQuery("#submit_event").click(addEvent);
	jQuery("#event_form").addClass('hidden');
	jQuery("#add_event").click(slideToggleEventForm);
	jQuery("#add_event").addClass('visible');
	jQuery("#cancel_event").click(slideToggleEventForm);

        jQuery("#message").slideUp(5000);


	jQuery(".deletion_form").addClass('hidden');
	jQuery(".del_event").click(function(){                  
                                  pos = this.id;

                                  jQuery("#deletion_form"+pos).slideDown();
                                  //jQuery("#cancel_deletion"+pos).click(slideToggleDelEventForm(id));
				  jQuery("#cancel_deletion"+pos).click(function(){

                                   jQuery("#deletion_form"+pos).slideUp();
				    return false;
                                   });
				  jQuery("#submit_deletion"+pos).click(function(){

                                  delEvent(pos)
					  
                                   });

                                   });



	statusBox();
}

jQuery(document).ready(prepareDocument);