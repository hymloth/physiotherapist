# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^gambler/', include('gambler.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    
    (r'^calendar/', include('django_calendar.urls')),


    (r'^', include('patient.urls')),
    
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': '/home/hymloth/Desktop/DJANGO/ileana/static'}),
    
    
)
