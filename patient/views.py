# -*- coding: utf-8 -*-
# Create your views here.
from django.shortcuts import get_object_or_404, render_to_response
from ileana.patient.models import Patient, Event, SubjectiveExamination, PhysicalExamination , GraphNote, Management
from django.template import RequestContext
from ileana import settings
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from django.utils import simplejson
from django.db.models import Max, Q


from django.core import serializers


from patient.forms import EventForm , PatientForm , SEXAMForm , CEXAMForm , ManagementForm
from django_calendar import DynamicCalendar
import datetime

from django.db import IntegrityError


def index(request, template_name="patient/index.html"):

      c = DynamicCalendar()
      calendar_html = c.generate_calendar()
      forday = datetime.date.today()
      events = Event.objects.filter(for_date__day = forday.day, for_date__month = forday.month , for_date__year = forday.year).order_by('for_date')
      event_form = EventForm()
      return render_to_response(template_name, locals(),context_instance=RequestContext(request))
      
      

def show_patient(request, id, template_name="patient/patient_history.html"):
      patient = get_object_or_404(Patient, pk=id)
      s_exams = SubjectiveExamination.objects.filter(patient=patient).order_by('on_visit')
      c_exams = []
      for s in s_exams:
	temp = PhysicalExamination.objects.filter(s_exam=s)
	if temp:
	  c_exams.append(temp)
	  
      c_exams = [c[0] for c in c_exams]

      management = Management.objects.filter(patient=patient)
      
      graphnotes = GraphNote.objects.filter(patient=patient) 
      notes = []  
      # visit http://www.sanisoft.com/blog/2008/05/26/img-notes-jquery-plugin/
      for g in graphnotes:
	json_dict = {"pid": "%s"%patient.pk,"nid": "%s"%g.pk, "x1": "%s"%g.x1 , 'y1': '%s'%g.y1, 'height': '%s'%g.height, 'width': '%s'%g.width,  'note': '%s'%g.note }
	notes.append(json_dict)
      notes = simplejson.dumps(notes)      
      

      return render_to_response(template_name, locals(),context_instance=RequestContext(request))


def search_patient(request,template_name="patient/search.html"):

  search = True
  patient_slug = request.GET.get('patient_name', '')
  if patient_slug != '':
      # hack at 4-4-2011, search must include phone or mobile
    q1 = Q(phone=patient_slug)  
    q2 = Q(mobile=patient_slug)
    q3 = Q(name__icontains=patient_slug)
    patients = Patient.objects.filter(q1|q2|q3)
  else:
    patients= ' '
  return render_to_response(template_name, locals(),context_instance=RequestContext(request))


def add_management(request, pid=None, mid=None, template_name="patient/management.html"):

  if pid:
      to_edit = True
      patient = get_object_or_404(Patient, pk=pid)
         
  if mid:
      to_edit = True
      m_instance = get_object_or_404(Management, pk=mid)
  else:
      to_edit = False
      m_instance = None
      
      
  if request.POST:
    to_edit = True
    management_form = ManagementForm(request.POST,instance=m_instance)      
    if management_form.is_valid():
      m = management_form.save(commit=False)
      m.patient = patient
      m.save()
      m_instance = m
      message = "ΟΙ ΑΛΛΑΓΕΣ ΣΑΣ ΠΡΑΓΜΑΤΟΠΟΙΗΘΗΚAΝ"	  
    else:
      to_edit = True
      message = management_form.errors.as_ul()	  
  else:     
    management_form = ManagementForm(instance=m_instance)
  
  return render_to_response(template_name, locals(),context_instance=RequestContext(request))
  
  
  

def add_c_exam(request, pid=None, sid=None, cid=None, template_name="patient/c_exam_form.html"):

  if pid:
      to_edit = True
      patient = get_object_or_404(Patient, pk=pid)
      
  if sid:
      to_edit = True
      s_instance = get_object_or_404(SubjectiveExamination, pk=sid)
  else:
      to_edit = False
      s_instance = None
      
  if cid:
      to_edit = True
      c_instance = get_object_or_404(PhysicalExamination, pk=cid)
  else:
      to_edit = False
      c_instance = None      

  if request.POST:
    to_edit = True
    c_exam_form = CEXAMForm(request.POST,instance=c_instance)      
    if c_exam_form.is_valid():
      c = c_exam_form.save(commit=False)
      c.s_exam = s_instance
      c.save()
      c_instance = c
      message = "ΟΙ ΑΛΛΑΓΕΣ ΣΑΣ ΠΡΑΓΜΑΤΟΠΟΙΗΘΗΚAΝ"	  
    else:
      to_edit = True
      message = c_exam_form.errors.as_ul()	  
  else:     
    c_exam_form = CEXAMForm(instance=c_instance)
  
  return render_to_response(template_name, locals(),context_instance=RequestContext(request))



def add_s_exam(request, pid=None, sid=None, template_name="patient/s_exam_form.html"):

  if pid:
      to_edit = True
      patient = get_object_or_404(Patient, pk=pid)

      
  if sid:
      to_edit = True
      s_instance = get_object_or_404(SubjectiveExamination, pk=sid)
  else:
      to_edit = False
      s_instance = None

  c_exams = PhysicalExamination.objects.filter(s_exam=s_instance)

  if request.POST:
    to_edit = True
    s_exam_form = SEXAMForm(request.POST,instance=s_instance)      
    if s_exam_form.is_valid():
      s = s_exam_form.save(commit=False)
      visit = SubjectiveExamination.objects.filter(patient=patient).aggregate(Max('on_visit'))['on_visit__max']
      if visit == None:
	visit = 0
      s.on_visit = visit + 1
      s.patient = patient
      s.save()
      s_instance = s
      message = "ΟΙ ΑΛΛΑΓΕΣ ΣΑΣ ΠΡΑΓΜΑΤΟΠΟΙΗΘΗΚAΝ"	  
    else:
      to_edit = True
      message = s_exam_form.errors.as_ul()	  
  else:     
    s_exam_form = SEXAMForm(instance=s_instance)
  
  return render_to_response(template_name, locals(),context_instance=RequestContext(request))
  





def add_patient(request, id=None, template_name="patient/main.html"):  
  if id:
      to_edit = True
      patient = get_object_or_404(Patient, pk=id)
  else:
      to_edit = False
      patient = None

  s_exams = SubjectiveExamination.objects.filter(patient=patient).order_by('-on_visit')[:1]
  if s_exams:
    c_exams = PhysicalExamination.objects.filter(s_exam=s_exams[0])
  
  management = Management.objects.filter(patient=patient)
  

  graphnotes = GraphNote.objects.filter(patient=patient) 
  notes = []  
  # visit http://www.sanisoft.com/blog/2008/05/26/img-notes-jquery-plugin/
  for g in graphnotes:
    json_dict = {"pid": "%s"%patient.pk,"nid": "%s"%g.pk, "x1": "%s"%g.x1 , 'y1': '%s'%g.y1, 'height': '%s'%g.height, 'width': '%s'%g.width,  'note': '%s'%g.note }
    notes.append(json_dict)
  notes = simplejson.dumps(notes)


  if request.POST:
    to_edit = True
    form = PatientForm(request.POST,instance=patient)      
    if form.is_valid():
      patient = form.save()
      message = "ΟΙ ΑΛΛΑΓΕΣ ΣΑΣ ΠΡΑΓΜΑΤΟΠΟΙΗΘΗΚAΝ"	  
    else:
      to_edit = True
      html = render_to_string(template_name,{'message': form.errors.as_ul(), 'form':PatientForm(instance=patient)}, context_instance=RequestContext(request))  
      return HttpResponse(html)	  
  else:     
    form = PatientForm(instance=patient)
    
  return render_to_response(template_name, locals(),context_instance=RequestContext(request))



def add_notegraph(request, template_name="patient/main.html"):

  if request.POST:
    x = float(request.POST.get('data[Note][x1]'))
    y = float(request.POST.get('data[Note][y1]'))
    height = float(request.POST.get('data[Note][height]'))
    width = float(request.POST.get('data[Note][width]'))
    description = request.POST.get('data[Note][note]')
    patient = int(request.POST.get('patient'))
    
    note1 = GraphNote()
    note1.x1 = x
    note1.y1 = y
    note1.height = height
    note1.width = width
    note1.note = description
    note1.patient = Patient.objects.get(pk=patient)
    note1.save()
    
    
    
    return HttpResponseRedirect('/patient/%s/' %patient)


def delete_notegraph(request,nid, template_name="patient/main.html"):
  
  if request.POST:
    note = get_object_or_404(GraphNote, pk=nid)
    note.delete()
    patient = int(request.POST.get('patient'))
    
    
    return HttpResponseRedirect('/patient/%s/' %patient)


def add_event(request):
  
  if request.POST:
    form = EventForm(request.POST)
    name = request.POST.get('name')
    desc = request.POST.get('description')
    patient = request.POST.get('patients')
    hour =int(request.POST.get('hour'))
    minute =int(request.POST.get('minute'))
    second =int(request.POST.get('second'))
    date = request.POST.get('date')
    date = date.split('-')
    event = Event()
    event.name = name
    event.description = desc
    event.for_date = datetime.datetime(int(date[0]),int(date[1]),int(date[2]),hour,minute,second)
    
    if patient:  
      p = Patient.objects.get(pk=int(patient))
      event.patients=p
    
    event.save()

  template = "patient/events.html"
  html = render_to_string(template, {'event': event, 'event_form': form })  
  response = simplejson.dumps({'success':'True', 'html': html})
  return HttpResponse(response,content_type='application/javascript; charset=utf-8')


    


def delete_event(request):
  
  postdata = request.POST.copy()
  form = EventForm(postdata)
  pkk = request.POST.get('idd')
  
  event = Event.objects.get(pk=int(pkk))
  event.delete()

  template = "patient/events.html"
  html = ''  
  response = simplejson.dumps({'success':'True', 'html': html})
  return HttpResponse(response,content_type='application/javascript; charset=utf-8')