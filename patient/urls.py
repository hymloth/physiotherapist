# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *

urlpatterns = patterns('ileana.patient.views',
  (r'^$', 'index', { 'template_name':'patient/index.html'}, 'patient_home'),
  

  (r'^history/(?P<id>\d+)/$','show_patient', {
'template_name':'patient/patient_history.html'},'patient_url'),

  (r'^patient/(?P<id>\d+)/$','add_patient', {
'template_name':'patient/patient_main.html'},'patient_url'),



  (r'^add_patient/(?P<id>\d+)/$','add_patient', {
'template_name':'patient/patient_main.html'},'add_patient_url'),

  (r'^add_patient/$','add_patient', {
'template_name':'patient/patient_main.html'},'add_patient_url'),



  (r'^add_s_exam/(?P<pid>\d+)/(?P<sid>\d+)/$','add_s_exam', {
'template_name':'patient/s_exam_form.html'},'add_s_exam_url'),

  (r'^add_s_exam/(?P<pid>\d+)/$','add_s_exam', {
'template_name':'patient/s_exam_form.html'},'add_s_exam_url'),



  (r'^add_c_exam/(?P<pid>\d+)/(?P<sid>\d+)/(?P<cid>\d+)/$','add_c_exam', {
'template_name':'patient/c_exam_form.html'},'add_c_exam_url'),

  (r'^add_c_exam/(?P<pid>\d+)/(?P<sid>\d+)/$','add_c_exam', {
'template_name':'patient/c_exam_form.html'},'add_c_exam_url'),


  (r'^add_management/(?P<pid>\d+)/(?P<mid>\d+)/$','add_management', {
'template_name':'patient/management.html'},'add_management'),

  (r'^add_management/(?P<pid>\d+)/$','add_management', {
'template_name':'patient/management.html'},'add_management'),


 (r'^notegraph/add/$', 'add_notegraph',{
 'template_name':'patient/patient_main.html'   },'add_notegraph'),
 
  (r'^notegraph/delete/(?P<nid>\d+)/$', 'delete_notegraph',{
 'template_name':'patient/patient_main.html'   },'delete_notegraph'),


  (r'^search_patient/$','search_patient', {
'template_name':'patient/search.html'},'search_patient_url'),

  

  (r'^event/add/$', 'add_event'),
  
  
  (r'^event/delete/$', 'delete_event'),


  #(r'^season/(?P<season_slug>[-\w]+)/$','show_season', {
#'template_name':'catalog/season.html'},'catalog_season'),

  #(r'^team/(?P<team_slug>[-\w]+)/(?P<season_slug>[-\w]+)$','show_team', {
#'template_name':'catalog/team.html'},'catalog_team'),

  #(r'^update/team/(?P<team_slug>[-\w]+)/(?P<season_slug>[-\w]+)$','update_team'),
  
  #(r'^update/season/(?P<season_slug>[-\w]+)/$','update_season'),
  
  #(r'^update/league/(?P<league_slug>[-\w]+)/$','update_league'),

  #(r'^search/','search'),
  
  #(r'^download/(?P<file_slug>[-\w]+)/$', 'create_pdf'),

)