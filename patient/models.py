# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

      
class Patient(models.Model):

    IMPR = 1
    STAT = 2
    WORS = 3
    STATE_CHOICES = (
        (IMPR, 'IMPROVING'),
        (STAT, 'STATIC'),
        (WORS, 'WORSENING'),
    )

    SEVERE = 1
    IRRITABLE = 2
    SEVIRR_CHOICES = (
        (SEVERE, 'SEVERE'),
        (IRRITABLE, 'IRRITABLE'),

    )

    name = models.CharField(unique=True,max_length=255) 
                            
    date_added = models.DateField(auto_now_add=True)
    age = models.CharField(max_length=3,blank=True,null=True)
    phone = models.CharField(max_length=30,blank=True,null=True)
    mobile = models.CharField(max_length=30,blank=True,null=True)
    
    behaviour =  models.TextField(max_length=30,blank=True,null=True)
    state = models.IntegerField(choices=STATE_CHOICES, default=STAT,blank=True,null=True)
    
    special_questions = models.CharField(max_length=255,blank=True,null=True)
    general_health = models.CharField(max_length=255,blank=True,null=True)
    weight_loss = models.CharField(max_length=255,blank=True,null=True)
    RA = models.CharField(max_length=255,blank=True,null=True)
    Drugs = models.CharField(max_length=255,blank=True,null=True)
    Steroids = models.CharField(max_length=255,blank=True,null=True)
    Anticoagulants = models.CharField(max_length=255,blank=True,null=True)
    xray = models.CharField(max_length=255,blank=True,null=True)
    cord_symptoms = models.CharField(max_length=255,blank=True,null=True)
    cauda_equina_symptoms = models.CharField(max_length=255,blank=True,null=True)
    VBI_symptoms = models.CharField(max_length=255,blank=True,null=True)
    
    relationship_of_symptoms =  models.TextField(max_length=30,blank=True,null=True)
    aggravating_factors_and_function =  models.TextField(max_length=30,blank=True,null=True)
    easing_factors =  models.TextField(max_length=30,blank=True,null=True)
    severity_irritability = models.IntegerField(choices=SEVIRR_CHOICES, default=SEVERE,blank=True,null=True)
    HPC =  models.TextField(max_length=30,blank=True,null=True)
    PMH =  models.TextField(max_length=30,blank=True,null=True)
    SHFH =  models.TextField(max_length=30,blank=True,null=True)

    

    class Meta:
        ordering = ['name']
    
    def __unicode__(self):
        return self.name
        
    @models.permalink
    def get_absolute_url(self):
        return ('patient_main', (), { 'patient_slug': self.slug })  
        
        
        
class SubjectiveExamination(models.Model):
  
  symptom = models.CharField(max_length=255)
  on_visit = models.IntegerField(max_length=4)
  climax = models.IntegerField(choices=((i,"%s"%i) for i in range(1,11)),blank=True,null=True)
  severe = models.BooleanField(default=False)
  irritable = models.BooleanField(default=False)
  short_of_production = models.TextField(max_length=255,blank=True,null=True)
  point_of_onset_or_increase = models.TextField(max_length=255,blank=True,null=True)
  partial_reproduction = models.TextField(max_length=255,blank=True,null=True)
  repeat = models.TextField(max_length=255,blank=True,null=True)
  alter_speed = models.TextField(max_length=255,blank=True,null=True)
  combine = models.TextField(max_length=255,blank=True,null=True)
  sustain = models.TextField(max_length=255,blank=True,null=True)
  other = models.TextField(max_length=255,blank=True,null=True)
  precautions_contraindications = models.TextField(max_length=255,blank=True,null=True)
  other_factors_contributing = models.TextField(max_length=255,blank=True,null=True)
  symptomatic_area = models.CharField(max_length=255,blank=True,null=True)
  structures_under_area = models.CharField(max_length=255,blank=True,null=True)
  structures_which_can_refer_to_area = models.CharField(max_length=255,blank=True,null=True)
  supporting_evidence = models.CharField(max_length=255,blank=True,null=True)
  mechanism = models.TextField(max_length=255,blank=True,null=True)
  factors_to_explore_in_physical_examination = models.TextField(max_length=255,blank=True,null=True)
  
  patient = models.ForeignKey(Patient)

  def __unicode__(self):
      return "SU(%s) %s"  %(self.on_visit,self.symptom)  
      
      

class PhysicalExamination(models.Model):
  
  mechanism = models.TextField(max_length=255,blank=True,null=True)
  climax = models.IntegerField(choices=((i,"%s"%i) for i in range(1,11)),blank=True,null=True)
  clinical_diagnosis = models.TextField(max_length=1000,blank=True,null=True)
  contributing_factors_addressing = models.TextField(max_length=1000,blank=True,null=True)
  severe = models.BooleanField(default=False)
  irritable = models.BooleanField(default=False)
  neurological_examination = models.TextField(max_length=255,blank=True,null=True)
  precautions_contraindications = models.TextField(max_length=255,blank=True,null=True)



  s_exam = models.ForeignKey(SubjectiveExamination)


  def __unicode__(self):
      return "PH(%s) %s"  %(self.s_exam.on_visit, self.s_exam.symptom)   

class Management(models.Model):
  
  physical_tests = models.TextField(max_length=555,blank=True,null=True)
  expected_findings = models.TextField(max_length=555,blank=True,null=True)
  unexpected_findings = models.TextField(max_length=555,blank=True,null=True)
  subjective_reassesment_asterisks = models.TextField(max_length=555,blank=True,null=True)
  physical_reassesment_asterisks = models.TextField(max_length=555,blank=True,null=True)
  treatment_plan_for_source_of_symptoms = models.TextField(max_length=555,blank=True,null=True)
  treatment_plan_for_contributing_factors = models.TextField(max_length=555,blank=True,null=True)
  goals_for_discharge =  models.TextField(max_length=555,blank=True,null=True)
  first_choice_of_treatment = models.TextField(max_length=555,blank=True,null=True)
  expected_response = models.TextField(max_length=555,blank=True,null=True)
  second_visit_treatment = models.TextField(max_length=555,blank=True,null=True)
  advice = models.TextField(max_length=555,blank=True,null=True)
  second_visit_examination = models.TextField(max_length=555,blank=True,null=True)
  third_visit_examination = models.TextField(max_length=555,blank=True,null=True)
  condition_of_patient = models.TextField(max_length=555,blank=True,null=True)
  overall_prognosis = models.TextField(max_length=555,blank=True,null=True)
  
  after_third_attendance_understanding = models.TextField(max_length=555,blank=True,null=True)
  after_third_attendance_clues = models.TextField(max_length=555,blank=True,null=True)
  
  after_discharge_understanding = models.TextField(max_length=555,blank=True,null=True)
  self_education = models.TextField(max_length=555,blank=True,null=True)
      
  patient = models.ForeignKey(Patient)
  
  def __unicode__(self):
      return "MANAGEMENT FOR %s"  % self.patient   
      
      
class GraphNote(models.Model):
  
  patient = models.ForeignKey(Patient)    
  x1 = models.FloatField(max_length=255,blank=True,null=True)
  y1 = models.FloatField(max_length=255,blank=True,null=True)
  height = models.FloatField(max_length=255,blank=True,null=True)
  width = models.FloatField(max_length=255,blank=True,null=True)
  note = models.CharField(max_length=255,blank=True,null=True)
  
  def __unicode__(self):
    return "(%s) %s"  %(self.patient, self.note)
        
class Event(models.Model):
  
    name = models.CharField(max_length=255)
    date_added = models.DateTimeField(auto_now_add=True)
    for_date = models.DateTimeField(blank=True,null=True)
    description = models.CharField(max_length=255)
    
    patients = models.ForeignKey(Patient,blank=True,null=True)
    
    class Meta:
        ordering = ['for_date']
    
    def __unicode__(self):
        return self.name    