# -*- coding: utf-8 -*-
from django.contrib import admin
from ileana.patient.models import Patient , Event , SubjectiveExamination, PhysicalExamination, GraphNote, Management

class ManagementAdmin(admin.ModelAdmin):
  pass
    
# registers your product model with the admin site
admin.site.register(Management, ManagementAdmin)

class GraphNoteAdmin(admin.ModelAdmin):
  pass
    
# registers your product model with the admin site
admin.site.register(GraphNote, GraphNoteAdmin)

class PhysicalExaminationAdmin(admin.ModelAdmin):
  pass
    
# registers your product model with the admin site
admin.site.register(PhysicalExamination, PhysicalExaminationAdmin)

class SubjectiveExaminationAdmin(admin.ModelAdmin):
  pass
    
# registers your product model with the admin site
admin.site.register(SubjectiveExamination, SubjectiveExaminationAdmin)

class PatientAdmin(admin.ModelAdmin):
  pass
    
# registers your product model with the admin site
admin.site.register(Patient, PatientAdmin)

class EventAdmin(admin.ModelAdmin):
  pass
    
# registers your product model with the admin site
admin.site.register(Event, EventAdmin)